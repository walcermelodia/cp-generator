package ru.mts.bank.cp.generator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CurrencyPair {
    private final String pairName;
    private final Double bid;
    private final Double ask;
    private final String creationDate;
}
