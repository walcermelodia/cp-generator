package ru.mts.bank.cp.generator.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Setter
@Getter
@ConfigurationProperties(prefix = "currency")
public class ConfigProperties {
    private Map<String, Double> pairs;
}
