package ru.mts.bank.cp.generator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
@EnableScheduling
public class SchedulerConfig {
    private final static int DEFAULT_CORE_POOL_SIZE = 30;
    private final static int DEFAULT_MAX_POOL_SIZE = 30;
    private final static int DEFAULT_QUEUE_CAPACITY = 500;
    private final static String DEFAULT_THREAD_NAME = "CP-THREAD-";
    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(DEFAULT_CORE_POOL_SIZE);
        executor.setMaxPoolSize(DEFAULT_MAX_POOL_SIZE);
        executor.setQueueCapacity(DEFAULT_QUEUE_CAPACITY);
        executor.setThreadNamePrefix(DEFAULT_THREAD_NAME);
        executor.initialize();
        return executor;
    }
}
