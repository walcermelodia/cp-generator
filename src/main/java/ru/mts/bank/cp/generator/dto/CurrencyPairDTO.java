package ru.mts.bank.cp.generator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.mts.bank.cp.generator.model.CurrencyPair;

import java.util.List;

@AllArgsConstructor
@Data
public class CurrencyPairDTO {
    private String pairName;
    private int count;
    private final List<CurrencyPair> currencyPairs;
}
