package ru.mts.bank.cp.generator.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mts.bank.cp.generator.model.CurrencyPair;
import ru.mts.bank.cp.generator.repository.CurrencyPairRepository;

@Component
@RequiredArgsConstructor
public class SaveCurrencyPair {
    private final CurrencyPairRepository currencyPairRepository;
    @Async
    public void save(String name, CurrencyPair currencyPair) {
        currencyPairRepository.save(name, currencyPair);
    }
}
