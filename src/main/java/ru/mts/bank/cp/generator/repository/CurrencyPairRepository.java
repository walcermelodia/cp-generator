package ru.mts.bank.cp.generator.repository;

import org.springframework.stereotype.Component;
import ru.mts.bank.cp.generator.model.CurrencyPair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CurrencyPairRepository {
    private final static int DEFAULT_QUEUE_CAPACITY = 100;
    private final ConcurrentHashMap<String, ArrayBlockingQueue<CurrencyPair>> data = new ConcurrentHashMap<>();

    public Optional<List<CurrencyPair>> find(String namePair) {
        var result = data.get(namePair);
        if (Objects.isNull(result)) {
            return Optional.empty();
        }
        return Optional.of(new ArrayList<>(data.get(namePair)));
    }

    public void save(String key, CurrencyPair currencyPair) {
        data.putIfAbsent(key, new ArrayBlockingQueue<>(DEFAULT_QUEUE_CAPACITY));
        synchronized (data.get(key)) {
            if (!data.get(key).offer(currencyPair)) {
                data.get(key).poll();
                data.get(key).add(currencyPair);
            }
        }
    }
}
