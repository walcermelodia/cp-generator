package ru.mts.bank.cp.generator.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.mts.bank.cp.generator.config.ConfigProperties;
import ru.mts.bank.cp.generator.model.CurrencyPair;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class ScheduledTask {
    private final ConfigProperties configProperties;
    private final CurrencyPairGenerator currencyPairGenerator;
    private final SendMessage sendMessage;
    private final SaveCurrencyPair saveCurrencyPair;
    @Scheduled(fixedDelay = 1000)
    public void executeTask() throws JsonProcessingException {
        List<CurrencyPair> currencyPairs = configProperties.getPairs().entrySet().stream()
                .map(e -> currencyPairGenerator.generateCurrencyPair(e.getKey(), e.getValue()))
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        log.info("add cp");
        for (var pair : currencyPairs) {
            saveCurrencyPair.save(pair.getPairName(), pair);
            sendMessage.send(pair);
        }
        log.info("complete add");
    }
}
