package ru.mts.bank.cp.generator.mapper;

import org.springframework.stereotype.Component;
import ru.mts.bank.cp.generator.dto.CurrencyPairDTO;
import ru.mts.bank.cp.generator.model.CurrencyPair;

import java.util.List;

@Component
public class CurrencyPairMapper {
    public CurrencyPairDTO modelToDto(List<CurrencyPair> currencyPairList) {
        return new CurrencyPairDTO(
                currencyPairList.get(0).getPairName(),
                currencyPairList.size(),
                currencyPairList);
    }
}
