package ru.mts.bank.cp.generator.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mts.bank.cp.generator.model.CurrencyPair;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

@Component
@RequiredArgsConstructor
@Slf4j
public class CurrencyPairGenerator {
    private final static double EPS = 0.3;
    private final static double SCALE = 10000;
    private final DateTimeFormatter dtf;
    @Async
    public CompletableFuture<CurrencyPair> generateCurrencyPair(String title, double origin) {
        log.info("Start create " + title);
        double bound = origin + EPS;
        double a = Math.round(ThreadLocalRandom.current().nextDouble(origin, bound) * SCALE) / SCALE;
        double b = Math.round(ThreadLocalRandom.current().nextDouble(origin, bound) * SCALE) / SCALE;
        double bid = Math.max(a, b);
        double ask = Math.min(a, b);
        return CompletableFuture.completedFuture(
                new CurrencyPair(
                        title,
                        bid,
                        ask,
                        dtf.format(LocalDateTime.now())));
    }
}
