package ru.mts.bank.cp.generator.exception;

public class CurrencyPairNotFoundException extends RuntimeException {
    public CurrencyPairNotFoundException(String message) {
        super(message);
    }
}
