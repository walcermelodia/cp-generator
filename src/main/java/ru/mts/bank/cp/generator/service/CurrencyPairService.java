package ru.mts.bank.cp.generator.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mts.bank.cp.generator.dto.CurrencyPairDTO;
import ru.mts.bank.cp.generator.exception.CurrencyPairNotFoundException;
import ru.mts.bank.cp.generator.mapper.CurrencyPairMapper;
import ru.mts.bank.cp.generator.repository.CurrencyPairRepository;

@Service
@RequiredArgsConstructor
public class CurrencyPairService {
    private final CurrencyPairMapper currencyPairMapper;
    private final CurrencyPairRepository currencyPairRepository;
    public CurrencyPairDTO findAllByName(String title) {
        return currencyPairRepository.find(title)
                .map(currencyPairMapper::modelToDto)
                .orElseThrow(() -> new CurrencyPairNotFoundException(String.format("Currency Pair: %s not exists", title)));
    }
}
