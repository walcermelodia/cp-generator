package ru.mts.bank.cp.generator.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mts.bank.cp.generator.dto.CurrencyPairDTO;
import ru.mts.bank.cp.generator.service.CurrencyPairService;

@RestController
@RequestMapping("api/v1")
@RequiredArgsConstructor
public class CurrencyPairController {
    private final CurrencyPairService currencyPairService;

    @GetMapping(value = "/pairs/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CurrencyPairDTO findAllByName(@PathVariable("name") String name) {
        return currencyPairService.findAllByName(name);
    }
}
