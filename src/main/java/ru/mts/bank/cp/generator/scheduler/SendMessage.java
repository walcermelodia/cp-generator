package ru.mts.bank.cp.generator.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mts.bank.cp.generator.model.CurrencyPair;

@Component
@RequiredArgsConstructor
public class SendMessage {
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @Async
    public void send(CurrencyPair currencyPair) throws JsonProcessingException {
        String json = objectMapper.writeValueAsString(currencyPair);
        rabbitTemplate.setExchange("direct-exchange");
        rabbitTemplate.convertAndSend("currency-pair", json);
    }

}
