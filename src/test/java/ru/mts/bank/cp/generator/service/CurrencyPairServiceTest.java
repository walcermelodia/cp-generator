package ru.mts.bank.cp.generator.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.mts.bank.cp.generator.dto.CurrencyPairDTO;
import ru.mts.bank.cp.generator.exception.CurrencyPairNotFoundException;
import ru.mts.bank.cp.generator.mapper.CurrencyPairMapper;
import ru.mts.bank.cp.generator.model.CurrencyPair;
import ru.mts.bank.cp.generator.repository.CurrencyPairRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CurrencyPairServiceTest {
    private final static String NAME = "EURUSD";
    @Mock
    private CurrencyPairMapper mapper;

    @Mock
    private CurrencyPairRepository repository;

    @InjectMocks
    private CurrencyPairService service;


    @Test
    void findAllByName_shouldThrowException() {
        final CurrencyPairService actual = mock(CurrencyPairService.class);

        doThrow(CurrencyPairNotFoundException.class)
                .when(actual)
                .findAllByName(NAME);

        assertThrows(CurrencyPairNotFoundException.class, () -> actual.findAllByName(NAME));
    }

    @Test
    void findAllByName_shouldFindAllCurrencyPairs() {
        final CurrencyPair pair = mock(CurrencyPair.class);
        List<CurrencyPair> pairs = List.of(pair);
        Optional<List<CurrencyPair>> pairsOptional = Optional.of(pairs);
        CurrencyPairDTO dto = mock(CurrencyPairDTO.class);
        when(repository.find(NAME)).thenReturn(pairsOptional);
        when(mapper.modelToDto(pairs)).thenReturn(dto);

        CurrencyPairDTO actual = service.findAllByName(NAME);

        assertEquals(dto, actual);
        verify(repository).find(NAME);
    }
}
