package ru.mts.bank.cp.generator.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.mts.bank.cp.generator.dto.CurrencyPairDTO;
import ru.mts.bank.cp.generator.mapper.CurrencyPairMapper;
import ru.mts.bank.cp.generator.model.CurrencyPair;
import ru.mts.bank.cp.generator.repository.CurrencyPairRepository;
import ru.mts.bank.cp.generator.service.CurrencyPairService;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CurrencyPairControllerTest {
    private final static String NAME = "EURUSD";

    @Mock
    private CurrencyPairService service;
    @InjectMocks
    private CurrencyPairController controller;

    @Test
    void findAllByName() {
        final CurrencyPairDTO dto = mock(CurrencyPairDTO.class);
        when(service.findAllByName(NAME)).thenReturn(dto);

        final CurrencyPairDTO actual = controller.findAllByName(NAME);

        assertNotNull(actual);
        assertEquals(dto, actual);
        verify(service).findAllByName(NAME);
    }
}