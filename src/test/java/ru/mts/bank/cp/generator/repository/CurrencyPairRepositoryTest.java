package ru.mts.bank.cp.generator.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mts.bank.cp.generator.model.CurrencyPair;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class CurrencyPairRepositoryTest {
    private final static String KEY = "EURUSD";
    private CurrencyPairRepository repository;
    @BeforeEach
    void setUp() {
        repository = new CurrencyPairRepository();
    }

    @Test
    void find_shouldFindCurrencyPair_whenExist() {
        final CurrencyPair currencyPair = mock(CurrencyPair.class);

        repository.save(KEY, currencyPair);
        final CurrencyPair actual = repository.find(KEY).get().get(0);

        assertNotNull(actual);
        assertEquals(currencyPair, actual);
    }

    @Test
    void find_shouldNotFind_whenKeyNotExist() {
        final Optional<List<CurrencyPair>> currencyPair = repository.find(KEY);

        assertFalse(currencyPair.isPresent());
    }


    @Test
    void save_shouldSaveLastCurrencyPair_whenCallMultipleTimes() {
        final CurrencyPair currencyPair = mock(CurrencyPair.class);
        final CurrencyPair lastCurrencyPair = mock(CurrencyPair.class);

        repository.save(KEY, currencyPair);
        repository.save(KEY, lastCurrencyPair);
        final CurrencyPair actual = repository.find(KEY).get().get(1);

        assertNotNull(actual);
        assertEquals(lastCurrencyPair, actual);
    }
}